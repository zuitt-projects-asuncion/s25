/*

	Activity: (database: session25)

	//Aggregate to count the total number of items supplied by Red Farms ($count stage)

	//Aggregate to count the total number of items with price greater than 50. ($count stage)

	//Aggregate to get the average price of all fruits that are onSale per supplier.($group)

	//Aggregate to get the highest price of fruits that are onSale per supplier. ($group)

	//Aggregate to get the lowest price of fruits that are onSale per supplier. ($group)


	//save your query/commands in the activity.js

*/
	//Aggregate to count the total number of items supplied by Red Farms ($count stage)
db.fruits.aggregate([
	{
		$match: {"supplier" : "Red Farms Inc."}
	},
	{
		$count: "stocks"
	} 
])


	//Aggregate to count the total number of items with price greater than 50. ($count stage)
db.fruits.aggregate([
	{
		$match: {"price" : {$gt: 50}}
	},
	{
		$count: "stocks"
	} 
])

	//Aggregate to get the average price of all fruits that are onSale per supplier.($group)
db.fruits.aggregate([
	{
		$match: {"onSale" : true}
	},
	{
		$group: {_id : "$supplier", avgPrice: {$avg:"$price"}}
	}	
])

	//Aggregate to get the highest price of fruits that are onSale per supplier. ($group)
db.fruits.aggregate([
	{
		$match: {"onSale": true}
	},	
	{
		$group: {_id : "$supplier", maxPrice: {$max:"$price"}}
	}
])

	//Aggregate to get the lowest price of fruits that are onSale per supplier. ($group)
db.fruits.aggregate([
	{
		$match: {"onSale": true}
	},	
	{
		$group: {_id : "$supplier", minPrice: {$min:"$price"}}
	}
])